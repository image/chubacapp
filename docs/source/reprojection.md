# Reprojection

Reproject annotations on 3D models. See reprojection article (in review process).

## Process

See the article for more details on the reprojection process.

## Required

- Reconstructed model (.ply) and it's associated sfm json file (.json)
- Biigle annotation report

## Parameters

- wholeframe only: All images from the sfm file will be reprojected (outline reprojection). The annotation report will be ignored.


## Output

- The output are 3 pickle files corresponding to Pandas dataframe:
    - points.pkl
        Reprojected points annotations. Each row correspond to a single annotation
        columns: 
            points: point annotation 3D coordinates
            label: point annotation label
            label_hier: point annotation label hierarchy
            filename: image filename of the annotation
            ann_id: annotation id
            radius: deprecated, to be removed
    - line.pkl
        Reprojected line annotations. Each row correspond to a single annotation
        columns: 
            points: list of 3D points of the annotation line
            label
            label_hier
            filename
            ann_id
    - polygons.pkl
        Reprojected polygon annotations. Each row correspond to a single annotation. For video annotations, one row is created for every frame.
        columns: 
            points: list of 3D points of the annotation polygon
            label
            label_hier
            filename
            ann_id
            misses: number of points of the polygons that have failed reprojection (missed the 3D model)


