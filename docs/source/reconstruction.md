# Reconstruction

The reconstruction tool allows to reconstruct textured 3d models from images.
It's based on [Colmap](https://colmap.github.io/), [openMVS](https://github.com/cdcseacave/openMVS) and [mvs-texturing](https://github.com/nmoehrle/mvs-texturing/tree/master).

## Process

- Extract features from all images
- Match features between images and verify geometry
- Use these matched features to reconstruct structure and motion
- Augment the number of points with dense reconstruction
- Create a 3D mesh from the points
- Texture the mesh using the images

## Required

- Images
- camera model
- navigation

## Parameters

![](images/reconstruction.png)

- **CPU feature:** Use CPU rather than GPU. Required if you haven't a cuda-enabled graphic card. See installation


- **Feature matching:**
Which feature match mode to perform. you can perform multiple matching. 
See [Colmap feature matching](https://colmap.github.io/tutorial.html#feature-matching-and-geometric-verification) for more details.
After all match scenarios, a transitive matching is performed.

- **Ignore two-views geometries:**
Ignore two-views during the reconstruction process. If the overlap between images is low, keeping two views can help.

- **Image scaling:**
How much to scale down images during the dense reconstruction part. 1 is no scale down. 
For HD image, use 2 to 4 depending on the amount of memory that you have
If you use large images (4k...), we recommend setting it to 6 to 8.

- **Decimation:** 
How much to decimate your mesh before texturing. 1 is no decimation, 0 is fully decimated. 
0.4 to 0.6 is a good value to spare memory.

- **Skip sparse reconstruction:**
Use this if the reconstruction failed or crashed after the sparse reconstruction step. 
You will jump directly after sparse reconstruction. 

## Output

- Textured mesh
- Optical navigation: sfm_data.json
- ...
