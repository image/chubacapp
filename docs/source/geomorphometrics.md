# Geomorphometrics

Computation of several geomorphometrics at several scales for the reconstructed models.
Uses the [CloudComPy library](https://github.com/CloudCompare/CloudComPy).

## Process

- **Slope:**
- **Roughness:**
- **Aspect:**
- **BPI and TRI:**

For BPI and TRI computation, we use custom algorithms derived from Wilson et al., 2007:

$$TRI_{(n)}=\frac{\sum_{i=0}^{N}|Z_i-Z|}{N-1}$$

$$BPI_{(n)}=Z - \frac{\sum_{i=0}^{N}Z_i}{N}$$


- **Mean and gaussian curvature:**
- **VRM:**

![VRM calculation](images/vrm.PNG)

## Required

- Reconstructed model

## Parameters

- **3D model:** The 3D model for which to compute geomorphometrics

- **Scales:** Different scales (in meters) for which to compute geomorphometrics

