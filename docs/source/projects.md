# Projects

Chubacapps works with projects to store information on your camera parameters, image and navigation location, results...
It is stored as a project_name.json file in your project directory. You can open it and modify it as any regular text file.

## Manual modification

If needed, you can modify your project using directly the project json file. 

## Projects content

### Inputs

### Outputs

