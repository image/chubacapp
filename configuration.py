import os
import navigation
import video
import utility
import pandas as pd
import numpy as np
import datetime
import logging
from UI.navigation_ui import Ui_Dialog
from UI.add_video import Ui_Dialog as Ui_Dialog_add_video
from UI.add_image_ui import Ui_Dialog as Ui_Dialog_add_img
from UI.add_3D_model_ui import Ui_Dialog as Ui_Dialog_add_3D
from UI.annotations_ui import Ui_Dialog as Ui_Dialog_annotations
from PyQt5.QtWidgets import (QDialog, QFileDialog, QMessageBox, QScrollArea, QWidget, QVBoxLayout, QLabel)
from PyQt5.QtCore import QCoreApplication
from video_annotations import report_type

inputs = 0

"""
Configuration module for Chubacapp.

This module provides functions and classes for configuring the Chubacapp application. It includes functions for 
retrieving vocabulary tree files, adding images, videos, 3D models, and navigation data to the project configuration, 
updating the user interface, and launching dialogs for adding navigation, images, videos, 3D models, and annotations.

Functions:
    get_vocab_tree: Get the vocabulary tree files from the specified directory.
    add_images: Add images to the project configuration.
    add_videos: Add videos to the project configuration.
    add_3d_model: Add a 3D model to the project configuration.
    set_camera_model: Set the camera model in the project configuration.
    add_navigation: Add navigation data to the project configuration.
    update_interface: Update the user interface with the project configuration.
    AddNavigation: Dialog for adding navigation data.
    AddImage: Dialog for adding images.
    AddVideo: Dialog for adding videos.
    Add3Dmodel: Dialog for adding 3D models.
    AddAnnotations: Dialog for adding annotations.
"""


def get_vocab_tree():
    """
Get the vocabulary tree files from the specified directory.

This function retrieves the vocabulary tree files from the specified directory. It creates a dictionary where the keys are the file names and the values are the file paths. Only files with the ".bin" extension are included in the dictionary.

Returns:
    dict: A dictionary of vocabulary tree files, where the keys are the file names and the values are the file paths.
"""

    vocab_dir = r"configurations/vocab_trees"
    file_list = os.listdir(vocab_dir)
    tree_dict = {}
    for tree in file_list:
        tree_path = os.path.join(vocab_dir, tree)
        if tree_path.endswith(".bin"):
            tree_dict[tree] = tree_path
    return tree_dict

def add_images(project_config, image_path):
    count = sum(bool(file.endswith(".jpg"))
            for file in os.listdir(image_path))
    if count != 0:
        project_config['inputs']['image'] = True
        project_config['inputs']['image_path'] = image_path
    else:
        os.chdir(project_config['project_directory'])
        logging.basicConfig(filename=project_config['name'] + ".log", filemode="a")
        logging.error("Images : No image in this folder")
        print('Images : No image in this folder')

def add_videos(project_config, video_dir):
    project_config['inputs']['video'] = True
    project_config['inputs']['video_path'] = video_dir

def add_3d_model(project_config, model_path, sfm_path):
    project_config['outputs']['3D_model'] = True
    project_config['outputs']['3D_model_path'] = model_path
    project_config['outputs']['sfm_data_path'] = sfm_path


def set_camera_model(project_config, camera):
    project_config['inputs']['camera_model'] = True
    project_config['inputs']['camera_model_path'] = camera

def check_navigation_table(table,qt):
    column_name = ["date", "time", "camera", "name", "lat", "lon", "depth", "alt", "heading", "pitch", "roll"]
    if table.columns.tolist() != column_name:
        column_concerned =  list(set(table.columns.tolist()) - set(column_name))

        print(f"The following columns : {column_concerned} are not present in the dataframe")
        os.chdir(qt.project_config['project_directory'])
        logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
        logging.error(f"Navigation : The following columns : {column_concerned} are not present in the dataframe")

        msg_box_name = QMessageBox()
        msg_box_name.setIcon(QMessageBox.Warning)
        msg_box_name.setWindowTitle("Navigation Data")
        msg_box_name.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg_box_name.setText(f"The following columns : {column_concerned} are not present in the dataframe")
        retval = msg_box_name.exec_()
    else:
        os.chdir(qt.project_config['project_directory'])
        logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
        logging.info("Navigation : Column names are matching the excepted ones")
        print("Column names are matching the excepted ones")

    if np.where(pd.isnull(table))[1]:
        list_of_columns = np.unique(np.where(pd.isnull(table))[1].tolist()).tolist()
        column_concerned = []
        for i in list_of_columns:
            column_concerned.append(column_name[i])
        print(f"The following columns : {column_concerned} contained some empty cells or non data value")
        os.chdir(qt.project_config['project_directory'])
        logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
        logging.error(f"Navigation : The following columns : {column_concerned} contained some empty cells or non data value")

        msg_box_name = QMessageBox()
        msg_box_name.setIcon(QMessageBox.Warning)
        msg_box_name.setWindowTitle("Navigation Data")
        msg_box_name.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg_box_name.setText(f"The following columns : {column_concerned} contained some empty cells or non data value")
        retval = msg_box_name.exec_()
    else:
        print('No empty cell in the navigation table')
        os.chdir(qt.project_config['project_directory'])
        logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
        logging.info("Navigation : No empty cell in the navigation table")

def navigation_table_outliers(table,qt):
    outliers_list = []

    # Date - Time
    """combined_date_time = table['date'] + " " + table['time']
    date_data = pd.to_datetime(combined_date_time.unique()).tolist()
    continuity = []
    for i in np.arange(1,len(date_data)):
        continuity.append((date_data[i]-date_data[i-1]).total_seconds())
    if all(val > 0 for val in continuity) is False:
        outliers_list.append('date')
        outliers_list.append('time')"""

    # Camera
    camera_data = np.unique(table['camera']).tolist()
    if len(camera_data) != 1:
        outliers_list.append('camera')

    # Latitude / Longitude
    latitude_data = list(table['lat'])
    if all(isinstance(val,float) for val in latitude_data) is False:
        outliers_list.append('latitude')
    if (max(latitude_data) >= 180) or (min(latitude_data) <= -180):
        outliers_list.append('latitude')

    longitude_data = list(table['lon'])
    if all(isinstance(val,float) for val in longitude_data) is False:
        outliers_list.append('longitude')
    if (max(longitude_data) >= 180) or (min(longitude_data) <= -180):
        outliers_list.append('longitude')

    # Depth
    depth_data = list(table['depth'])
    if min(depth_data)<0:
        outliers_list.append('depth')

    # Altitude
    altitude = list(table['alt'])
    if all(val > 0 for val in altitude) is True:
        outliers_list.append('altitude')

    # Heading
    heading_data = list(table['heading'])
    if (min(heading_data) < 0) or (max(heading_data) > 360):
        outliers_list.append('heading')

    # Pitch
    pitch_data = list(table['pitch'])
    if (min(pitch_data) < -90) or (max(pitch_data) > 90):
        outliers_list.append('pitch')

    # Roll
    roll_data = list(table['roll'])
    if (min(roll_data) < -180) or (max(roll_data) > 180):
        outliers_list.append('roll')

    if outliers_list:
        print(f"Some values are out of range or in the wrong format in the following columns : {outliers_list}")
        os.chdir(qt.project_config['project_directory'])
        logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
        logging.error(f"Navigation : Some values are out of range or in the wrong format in the following columns : {outliers_list}")

        msg_box_name = QMessageBox()
        msg_box_name.setIcon(QMessageBox.Warning)
        msg_box_name.setWindowTitle("Navigation Data")
        msg_box_name.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg_box_name.setText(f"Some values are out of range or in the wrong format in the following columns : {outliers_list}")
        retval = msg_box_name.exec_()

class ScrollMessageBox(QMessageBox):
   def __init__(self, l, *args, **kwargs):
      QMessageBox.__init__(self, *args, **kwargs)
      self.setWindowTitle("Navigation Data")
      self.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
      scroll = QScrollArea(self)
      scroll.setWidgetResizable(True)
      self.content = QWidget()
      scroll.setWidget(self.content)
      lay = QVBoxLayout(self.content)
      for item in l:
         lay.addWidget(QLabel(item, self))
      self.layout().addWidget(scroll, 0, 0, 1, self.layout().columnCount())
      self.setStyleSheet("QScrollArea{min-width:300 px; min-height: 400px}")

def check_image_vs_navigation(qt):
    if qt.project_config['inputs']['navigation_path'] != '' and qt.project_config['inputs']['image_path'] != '':
        image_folder = qt.project_config['inputs']['image_path']
        navigation_file = qt.project_config['inputs']['navigation_path']

        list_of_images = [s for s in os.listdir(image_folder) if s.endswith('.jpg')]
        filename, nav_type = os.path.splitext(qt.project_config['inputs']['navigation_path'])

        if nav_type == ".dim2":
            navigation_table = navigation.parse_nav_file_dim2(qt.project_config['inputs']['navigation_path'])
        elif nav_type == '.csv':
            navigation_table = navigation.parse_csv(qt.project_config['inputs']['navigation_path'])

        list_of_images_nv = navigation_table['name']

        if list(set(list_of_images) - set(list_of_images_nv)):
            to_write = []
            to_write.append("The following images are not in the navigation dataframe :")
            to_write.extend(list(set(list_of_images_nv) - set(list_of_images)))
            msg_box_name = ScrollMessageBox(to_write)
            retval = msg_box_name.exec_()

            os.chdir(qt.project_config['project_directory'])
            logging.basicConfig(filename=qt.project_config['name'] + ".log", filemode="a")
            logging.error(f"Navigation : The following images are not in the navigation dataframe : {to_write}")

    else:
        return


def add_navigation(qt,project_config, navigation_path, nav_type):
    project_config['inputs']['navigation'] = nav_type
    project_config['inputs']['navigation_path'] = navigation_path

    if nav_type == "dim2":
        navigation_table = navigation.parse_nav_file_dim2(navigation_path)
    elif nav_type == 'csv':
        navigation_table = navigation.parse_csv(navigation_path)

    check_navigation_table(navigation_table,qt)
    navigation_table_outliers(navigation_table, qt)

    model_origin = navigation_table[["lat", "lon", "depth"]].iloc[0].tolist()
    project_config['model_origin'] = model_origin
    return navigation_table

#"date", "time", "camera", "name", "lat", "lon", "depth", "alt", "heading", "pitch", "roll"

def update_interface(qt):
    #qt.populate_cb()
    qt.camera_cb.setCurrentText(qt.project_config['inputs']['camera_model_path'])

    p_list = [qt.project_label, qt.image_label, qt.nav_label]
    l_list = [qt.project_config['project_directory']+'/'+qt.project_config['name'], qt.project_config['inputs']['image_path'], qt.project_config['inputs']['navigation_path']]
    for id in range(len(p_list)):
        p_list[id].setText(l_list[id])

    qt.project_label.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><a href=><span style=\" text-decoration: underline; color:#0000ff;\">"+qt.project_config['project_directory']+'/'+qt.project_config['name']+".json"+"</span></a></p></body></html>", None))
    qt.image_label.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><a href=><span style=\" text-decoration: underline; color:#0000ff;\">"+qt.project_config['inputs']['image_path']+"</span></a></p></body></html>", None))
    qt.nav_label.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><a href=><span style=\" text-decoration: underline; color:#0000ff;\">"+qt.project_config['inputs']['navigation_path']+"</span></a></p></body></html>", None))


class AddNavigation(QDialog, Ui_Dialog):
    global inputs
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.nav_type.addItems(['csv', "dim2"])
        self.project_config = qt.project_config
        self.nav_path_B.clicked.connect(self.select_nav)
        self.buttonBox.accepted.connect(self.launch)

    def select_nav(self):
        options = QFileDialog.Options()
        file_path = QFileDialog.getOpenFileName(self, "Open file", "", "*." + self.nav_type.currentText(), options=options)
        self.nav_path.setText(file_path[0])

    def launch(self):
        if os.path.exists(self.nav_path.text()):
            self.qt.nav_data = add_navigation(self.qt, self.project_config, self.nav_path.text(), self.nav_type.currentText())
            update_interface(self.qt)
            check_image_vs_navigation(self.qt)




class AddImage(QDialog, Ui_Dialog_add_img):
    global inputs
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config
        self.image_dir_B.clicked.connect(self.select_img_dir)
        self.buttonBox.accepted.connect(self.launch)

    def select_img_dir(self):
        dir_path = QFileDialog.getExistingDirectory(None, 'Open Directory', r"")
        self.image_dir.setText(dir_path)

    def launch(self):
        if self.image_dir.text() != "":
            add_images(self.project_config, self.image_dir.text())
            update_interface(self.qt)
            check_image_vs_navigation(self.qt)
        else:
            self.qt.pop_message("Warning", "Invalid entry")


class AddVideo(QDialog, Ui_Dialog_add_video):
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config
        self.video_dir_B.clicked.connect(self.select_video_dir)
        self.video_nav_B.clicked.connect(self.select_nav_file)
        self.buttonBox.accepted.connect(self.launch)

    def select_video_dir(self):
        dir_path = QFileDialog.getExistingDirectory(None, 'Open Directory', r"")
        self.video_dir.setText(dir_path)

    def select_nav_file(self):
        options = QFileDialog.Options()
        file_path = QFileDialog.getOpenFileName(self, "Open navigation file", "", "*.csv *.txt", options=options)
        self.video_nav.setText(file_path[0])

    def launch(self):
        if os.path.isdir(self.video_dir.text()):
            video_dir = self.video_dir.text()
            video_nav_path = self.video_nav.text()
            add_videos(self.project_config, video_dir)
            video_df = video.get_video_list(video_dir)
            extract = None
            if self.extract_img.checkState() and os.path.exists(video_nav_path):
                self.extract_images(extract, video_dir, video_df, video_nav_path)
            update_interface(self.qt)
        else:
            self.qt.pop_message("Warning", "Invalid entry")

    def extract_images(self, extract, video_dir, video_df, video_nav_path):
        #self.qt.normalOutputWritten("Starting image extraction...")
        print("Starting image extraction...")
        os.chdir(self.qt.project_config['project_directory'])
        logging.basicConfig(filename=self.qt.project_config['name'] + ".log", filemode="a")
        logging.info("Images : Starting image extraction...")

        if self.img_st.checkState():
            extract = {"method": 'st', "param": float(self.st.text())}
        elif self.img_overlap.checkState():
            extract = {"method": 'overlap', "param": float(self.overlap.text())}
        self.out_dir = os.path.join(video_dir, "images")
        utility.create_dir(self.out_dir)
        self.extract_image_thread = video.ExtractImageThread(video_df, self.out_dir, extract['param'], video_nav_path)
        self.extract_image_thread.finished.connect(self.end_extraction)
        self.extract_image_thread.nav_path.connect(self.get_nav_file)
        if extract['method'] == "st":
            self.extract_image_thread.start()

        add_images(self.project_config, self.out_dir)

    def get_nav_file(self, nav_path):
        nav_type = 'csv'
        nav = add_navigation(self.project_config, nav_path, nav_type)
        self.qt.nav_data = nav
        update_interface(self.qt)
        #self.qt.normalOutputWritten("Extraction ended successfully !")
        print("Extraction ended successfully !")
        os.chdir(self.qt.project_config['project_directory'])
        logging.basicConfig(filename=self.qt.project_config['name'] + ".log", filemode="a")
        logging.info("Navigation : Extraction ended successfully !")


    def end_extraction(self):
        print("Image extraction ended")
        os.chdir(self.qt.project_config['project_directory'])
        logging.basicConfig(filename=self.qt.project_config['name'] + ".log", filemode="a")
        logging.info("Image : Image extraction ended !")


class Add3Dmodel(QDialog, Ui_Dialog_add_3D):
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config
        self.model3D_B.clicked.connect(self.select_3D_file)
        self.sfm_data_B.clicked.connect(self.select_sfm_file)
        self.buttonBox.accepted.connect(self.launch)

    def select_3D_file(self):
        options = QFileDialog.Options()
        file_path = QFileDialog.getOpenFileName(self, "Open 3D file", "", "*.ply", options=options)
        self.model3D.setText(file_path[0])

    def select_sfm_file(self):
        options = QFileDialog.Options()
        file_path = QFileDialog.getOpenFileName(self, "Open sfm_data file", "", "*.json *.bin", options=options)
        self.sfm_data.setText(file_path[0])

    def launch(self):
        if self.model3D.text() != "" and self.sfm_data != "":
            add_3d_model(self.project_config, self.model3D.text(), self.sfm_data.text())
            update_interface(self.qt)
        else:
            self.qt.pop_message("Warning", "Invalid entry(s)")


class AddAnnotations(QDialog, Ui_Dialog_annotations):
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config
        self.annotations_B.clicked.connect(self.select_annotations_file)
        self.buttonBox.accepted.connect(self.launch)

    def select_annotations_file(self):
        options = QFileDialog.Options()
        file_path = QFileDialog.getOpenFileName(self, "Open annotation report", "", "*.csv", options=options)
        self.annotations.setText(file_path[0])

    def launch(self):
        file_path = self.annotations.text()
        if file_path != "" and os.path.exists(file_path):
            name = os.path.basename(file_path)
            rep_type = report_type(file_path)
            self.qt.project_config['inputs']['annotations'].append({"name": name, "rep_type": rep_type, "path": file_path})
            update_interface(self.qt)
        else:
            self.qt.pop_message("Warning", "Invalid entry")