import pyvista as pv
import numpy as np
import pandas as pd
from tqdm import tqdm
import os

res_dir = r"D:\annotation_test"
mesh_path = r"D:\annotation_test\mesh_textured.ply"

file_list = [
    os.path.join(res_dir, f"polygons_{i}.pkl") for i in range(15)
]


for file in tqdm(file_list[:1]):
    data = pd.read_pickle(file)
    data = data[data['label_hier'].str.startswith("Biota_Smartarid")]
    polygons = data['points'].to_list()
    mesh_list = []
    for i, polygon in enumerate(polygons):
        polygons_pts = pv.PolyData(np.array(polygon, dtype='float'))
        mesh = polygons_pts.delaunay_2d()
        mesh.save(os.path.join(res_dir, "out", f"{i}_polygons.ply"))
