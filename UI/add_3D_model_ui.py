# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '3Dmodel.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(380, 115)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Variable")
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.model3D = QtWidgets.QLineEdit(Dialog)
        self.model3D.setObjectName("model3D")
        self.horizontalLayout.addWidget(self.model3D)
        self.model3D_B = QtWidgets.QPushButton(Dialog)
        self.model3D_B.setMaximumSize(QtCore.QSize(30, 16777215))
        self.model3D_B.setObjectName("model3D_B")
        self.horizontalLayout.addWidget(self.model3D_B)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Variable")
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.sfm_data = QtWidgets.QLineEdit(Dialog)
        self.sfm_data.setObjectName("sfm_data")
        self.horizontalLayout_2.addWidget(self.sfm_data)
        self.sfm_data_B = QtWidgets.QPushButton(Dialog)
        self.sfm_data_B.setMaximumSize(QtCore.QSize(30, 16777215))
        self.sfm_data_B.setObjectName("sfm_data_B")
        self.horizontalLayout_2.addWidget(self.sfm_data_B)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept) # type: ignore
        self.buttonBox.rejected.connect(Dialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Select 3D model"))
        self.label.setText(_translate("Dialog", "3D model:"))
        self.model3D_B.setText(_translate("Dialog", "..."))
        self.label_2.setText(_translate("Dialog", "sfm data:"))
        self.sfm_data_B.setText(_translate("Dialog", "..."))
