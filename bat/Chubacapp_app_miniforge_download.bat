@echo
echo "Download CHUBACAPP"
powershell -Command "Invoke-WebRequest https://gitlab.ifremer.fr/image/chubacapp/-/archive/main/chubacapp-main.zip -Outfile CHUBACAPP.zip"
echo Done
cls 

echo "Download MiniForge"
powershell -Command "Invoke-WebRequest https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Windows-x86_64.exe -Outfile MiniForge.exe"
echo Done
cls 

MiniForge.exe
