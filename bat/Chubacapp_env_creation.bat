@echo
set /P chubacapp_path="Enter chubacapp main directory: "
cd %chubacapp_path%
set /P miniforge_path="Enter miniforge main directory: "
%miniforge_path%\_conda.exe env create --name=chubv2 --file=chubacappv2.yaml 
cd %miniforge_path%
cd condabin
conda.bat init