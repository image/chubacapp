import reprojection_database as rdb
import pandas as pd
import numpy as np
import geometry as geometry
import volume_filter as vf
from multiprocessing.pool import Pool
from tqdm import tqdm
from itertools import chain
import pyvista as pv
import math
from scipy.spatial.transform import Rotation as R


def plot_individuals(individuals: list[rdb.individual], mesh_path = None, plotter = None):
    individual_dict = {}
    for ind in tqdm(individuals):  # Pour chaque individus
        tracks = ind.tracks
        if len(tracks) != 0:
            if tracks[0].label != "bound":
                # Not possible for bounds
                list_annotations = list(chain.from_iterable([t.annotations for t in tracks]))
                individual_dict[ind.id] = [i.coordinates_file for i in list_annotations if i.volume_file is not None]

    if plotter is None:
        plotter = pv.Plotter()

    if mesh_path is not None:
        mesh = pv.read(mesh_path)
        plotter.add_mesh(mesh)

    key = 223
    val = individual_dict[key]
    color = list(np.random.choice(range(256), size=4))
    for array in val:
        color = list(np.random.choice(range(256), size=4))
        coords = np.load(array)
        plotter.add_lines(coords.astype('float'), connected=True, color=color, width=3)
    plotter.enable_eye_dome_lighting()
    #plotter.show()
    return plotter



def fit_plane(points):
    data = np.array(points)
    data_center = data.mean(axis=0)
    _, _, Vh = np.linalg.svd(data - data_center)
    i_vector = Vh[0]
    j_vector = Vh[1]
    normal = np.cross(i_vector, j_vector)

    # Create rotation matrix from basis vectors
    rotate_transform = np.eye(4)
    rotate_transform[:3, :3] = np.vstack((i_vector, j_vector, normal))
    rotate_transform_inv = rotate_transform.T

    return rotate_transform_inv

def get_grid(mesh):
    n = 10000  # for 2 random indices
    index = np.random.choice(mesh.points.shape[0], n, replace=False)
    rd_pts_1 = mesh.points[index]

    trsf_mat = fit_plane(rd_pts_1)

    pts = np.array([[i, j, 0] for i in range(-50, 50, 5) for j in range(-50, 50, 5)])

    point_cloud = pv.PolyData(pts)
    point_cloud.transform(trsf_mat)
    point_cloud.translate(mesh.center, inplace=True)
    return point_cloud



if __name__ == "__main__":
    print("computing volumes...")
    db_dir = r"D:\tests\databases"

    m = pv.read(r"D:\pl821-12\export\0\mesh.ply")
    normal = (0.670649409294, -0.634663820267, 0.383967667818)
    center = (-11.508852005005, 41.367221832275, -8.501032829285)

    clipped_mesh_1 = m.clip(normal,center, True)
    clipped_mesh_2 = m.clip(normal, center, False)

    pc1 = get_grid(clipped_mesh_1)
    pc2 = get_grid(clipped_mesh_2)


    plotter = pv.Plotter()
    plotter.add_mesh(clipped_mesh_1, color='red')
    plotter.add_mesh(pc1, color='blue')
    plotter.add_mesh(clipped_mesh_2, color='green')
    plotter.add_mesh(pc2, color='white')
    plotter.enable_eye_dome_lighting()
    plotter.show()

    plotter = pv.Plotter()
    plotter.add_mesh(clipped_mesh_1, color='red')
    plotter.add_mesh(clipped_mesh_2, color='green')
    plotter.enable_eye_dome_lighting()
    plotter.show()





    session, db_path = rdb.open_reprojection_database_session(db_dir, create=False)
    individuals = session.query(rdb.individual).all()
    plotter = plot_individuals(individuals)
    plotter.show()

    tracks = session.query(rdb.Tracks).all()
    track = tracks[0]
    list_annotations_file = [t.coordinates_file for t in track.annotations]



    final = np.array_split(list_annotations_file, 10)
    for l in final:
        plotter = pv.Plotter()
        plotter.add_mesh(m)

        for array in l:
            color = list(np.random.choice(range(256), size=4))
            coords = np.load(array)
            plotter.add_lines(coords.astype('float'), connected=True, color=color, width=3)
        plotter.show()
