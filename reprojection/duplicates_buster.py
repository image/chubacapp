from tqdm import tqdm
import reprojection_database as rdb
import pyvista as pv
from itertools import combinations
from multiprocessing.pool import Pool
from contacts_processes import search_contacts


def get_track_contacts(track: rdb.Tracks):
    contacts_1 = track.contact_1
    contacts_2 = track.contact_2
    track_contacts = [con.track_2_id for con in contacts_1]
    track_contacts.extend([con.track_1_id for con in contacts_2])
    return track_contacts


def get_contacts_per_labels(session):
    list_labels = [r[0] for r in session.query(rdb.Tracks.label).distinct()]
    try:
        list_labels.remove('bound')
    except ValueError:
        pass
    print(list_labels)
    for label in tqdm(list_labels):
        tracks = session.query(rdb.Tracks).filter(rdb.Tracks.label == label).all()
        annotations_dict = {track.id: track.annotations for track in tracks}
        volume_dict = {}
        for key, value in annotations_dict.items():
            volume_dict[key] = [pv.read(ann.volume_file) for ann in value]

        tracks_combinations = list(combinations(list(volume_dict.keys()), 2))

        print("Starting contact analysis... {} combinations to test".format(len(tracks_combinations)))

        args = [[volume_dict[t_comb[0]], volume_dict[t_comb[1]]] for t_comb in tracks_combinations]
        res = []
        with Pool() as pool:
            for result in pool.starmap(search_contacts, tqdm(args, total=len(args))):
                res.append(result)

        for i, t_comb in enumerate(tracks_combinations):
            if res[i]:
                track_1_id, track_2_id = t_comb
                contact = rdb.Contacts(track_1_id=track_1_id, track_2_id=track_2_id)
                session.merge(contact)
                session.commit()


def get_individuals(session):
    print("Evaluating number of annotations per tracks...")
    tracks = session.query(rdb.Tracks).filter(rdb.Tracks.label != 'bound').all()
    track_len_dict = {track.id: len(track.annotations) for track in tqdm(tracks)}
    tracks_id = [t.id for t in tracks]
    print("Done !")

    total = len(tracks_id)
    pbar = tqdm(total=total)
    p = 0

    while len(tracks_id) != 0:
        individual_id = -1
        current_id = tracks_id.pop(0)
        current_track = session.get(rdb.Tracks, current_id)
        images = [ann.image_name for ann in current_track.annotations]
        tracked = track_len_dict[current_id] != 1
        contacts = get_track_contacts(current_track)
        for contact in contacts:
            contact_track = session.get(rdb.Tracks, contact)
            individual = contact_track.individual
            if tracked:
                if individual is not None:
                    individual_id = individual.id
                    break
            else:
                contact_image = [ann.image_name for ann in contact_track.annotations][0]
                if contact_image == images[0]:  # Same image, different tracks
                    contacts.remove(contact)
                    continue
                if individual is not None:
                    individual_id = individual.id
                    break

        if individual_id == -1:
            new_individual = rdb.individual()
            session.add(new_individual)
            session.commit()
            individual_id = new_individual.id

        session.query(rdb.Tracks).filter(rdb.Tracks.id == current_id).update({rdb.Tracks.individual_id: individual_id},
                                                                             synchronize_session=False)
        for contact in contacts:
            session.query(rdb.Tracks).filter(rdb.Tracks.id == contact).update({rdb.Tracks.individual_id: individual_id},
                                                                              synchronize_session=False)
            try:
                tracks_id.remove(contact)
            except ValueError:
                pass
        session.commit()

        pbar.update((total - len(tracks_id)) - p)
        p = (total - len(tracks_id))


if __name__ == "__main__":
    db_dir = r"D:\tests\databases"
    session, path = rdb.open_reprojection_database_session(db_dir, False)
    #get_contacts_per_labels(session)
    get_individuals(session)

# for individual_id, tracks in individuals.items():
#     individual = rdb.individual(id=individual_id)
#     session.add(individual)
#     for track in tracks:
#         (session.query(rdb.Tracks)
#          .filter(rdb.Tracks.id == track)
#          .update({rdb.Tracks.individual_id: individual_id}, synchronize_session=False))
#         session.commit()
