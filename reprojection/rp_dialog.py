import os
import utility
import reproject
import logging
from UI.reprojection_ui import Ui_Dialog as Ui_Dialog_reproj
from PyQt5.QtWidgets import (QDialog, QFileDialog, QMessageBox)
import video_annotations as VA


class rpDialog(QDialog, Ui_Dialog_reproj):
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config
        self.hit_map_launch.clicked.connect(self.launch_get_hit_maps)
        self.reproject_launch.clicked.connect(self.launch_reprojection)

        self.reprojector = None
        self.annotations_list = None

        self.models_list = self.project_config['outputs']['3D_models']
        if len(self.models_list) != 0:
            item_list = [x['name'] for x in self.models_list]
            self.models3D.addItems(item_list)

        if self.project_config['outputs']['hit_maps'] != '':
            self.enable_reproject()

    def set_prog(self, val):
        self.progressBar.setValue(val)

    def enable_reproject(self):
        obj_to_enable = [self.label_3, self.annotation_cb, self.reproject_launch, self.wholeframe_only]
        for obj in obj_to_enable:
            obj.setDisabled(False)

        self.annotations_list = self.project_config['inputs']['annotations']
        if len(self.annotations_list) != 0:
            item_list = [x['name'] for x in self.annotations_list]
            self.annotation_cb.addItems(item_list)

        print("Reprojection : Enabled !")

        os.chdir(self.project_config['project_directory'])
        logging.basicConfig(filename=self.project_config['name'] + ".log", filemode="a")
        logging.info("Reprojection : Enabled !")


    def launch_get_hit_maps(self):
        project_path = self.project_config['project_directory']
        exp_path = utility.create_dir(os.path.join(project_path,'hit_maps'))
        model_name = self.models3D.currentText()
        model = None
        for x in self.models_list:
            if x['name'] == model_name:
                model = x
        if model is not None:
            model_path = model['model_path']
            sfm_path = model['sfm']

            self.reprojector = reproject.reprojector(model_path, sfm_path, exp_path)
            self.reprojector.prog_val.connect(self.set_prog)
            self.reprojector.finished.connect(self.end_get_hit_maps)
            self.reprojector.start()
        else:
            print("Reprojection : Error : No model selected")

            os.chdir(self.project_config['project_directory'])
            logging.basicConfig(filename=self.project_config['name'] + ".log", filemode="a")
            logging.error("Reprojection : Error : No model selected")

            msg_box_name = QMessageBox()
            msg_box_name.setIcon(QMessageBox.Critical)
            msg_box_name.setWindowTitle("Reprojection")
            msg_box_name.setText("Error : No model selected")
            retval = msg_box_name.exec_()

    def end_get_hit_maps(self):
        self.set_prog(0)
        self.project_config['outputs']['hit_maps'] = os.path.join(self.project_config['project_directory'], 'hit_maps')
        self.qt.normalOutputWritten("Hit maps generation ended \r")

        os.chdir(self.project_config['project_directory'])
        logging.basicConfig(filename=self.project_config['name'] + ".log", filemode="a")
        logging.error("Hit maps generation ended")

        #if len(self.project_config['inputs']['annotations']) != 0:
        self.enable_reproject()

    def launch_reprojection(self):
        hit_maps_path = self.project_config['outputs']['hit_maps']

        rep_name = self.annotation_cb.currentText()
        report = None
        for x in self.annotations_list:
            if x['name'] == rep_name:
                report = x
        wholeframe_only = self.wholeframe_only.isChecked()

        if report is not None and not wholeframe_only:
            rep_type = report['rep_type']
            rep_path = report['path']
            reprojected_annotation_path = os.path.join(self.project_config['project_directory'], "reprojected_annotations")
            utility.create_dir(reprojected_annotation_path)
            reprojection_config = {
                "name": "annotation",
                "rep_type": rep_type,
                "rep_path": rep_path,
                "output_path": reprojected_annotation_path
            }

        elif wholeframe_only:
            reprojected_annotation_path = os.path.join(self.project_config['project_directory'], "reprojected_image_imprints")
            utility.create_dir(reprojected_annotation_path)
            reprojection_config = {
                "name": "image_imprints",
                "output_path": reprojected_annotation_path
            }


        else:
            print("Reprojection : Error : No Annotation")

            os.chdir(self.project_config['project_directory'])
            logging.basicConfig(filename=self.project_config['name'] + ".log", filemode="a")
            logging.error("Reprojection : Error : No Annotation")

            msg_box_name = QMessageBox()
            msg_box_name.setIcon(QMessageBox.Critical)
            msg_box_name.setWindowTitle("Reprojection")
            msg_box_name.setText("Error : No Annotation")
            retval = msg_box_name.exec_()
            return 0

        self.annotations_reproject = reproject.AnnotationTo3d(self.project_config, hit_maps_path, reprojection_config)

        output_dir, db_path = self.annotations_reproject.reproject_annotations()
        if reprojection_config['name'] == "annotation":
            self.project_config['outputs']['reprojected_annotations'].append({
                "reprojected_annotation_dir": output_dir,
                "report_path": db_path,
                "report_type": rep_type,
                "report_name": rep_name,
            })
        elif reprojection_config['name'] == "image_imprint":
            self.project_config['outputs']['wholeframe_db_dir'] = db_path

        print("Reprojection done !")
