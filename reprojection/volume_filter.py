import os
import pandas as pd
import numpy as np
import pyvista as pv
import reprojection_database as rdb
from statistics import mean, stdev, median, quantiles

def mesh_to_volume(mesh):
    mesh.compute_normals(inplace=True)
    mesh.compute_normals(cell_normals=True, point_normals=False, inplace=True)
    normals = np.asarray(mesh['Normals'])
    vect = np.asarray([mean(normals[:, 0]), mean(normals[:, 1]), mean(normals[:, 2])])
    mesh_bottom = mesh.translate(-vect, inplace=True)
    return mesh_bottom.extrude(4 * vect, capping=True)


def extract_points_in_volume(point_cloud, vol):
    result = point_cloud.select_enclosed_points(vol, check_surface=False)
    return point_cloud.extract_points(
        result['SelectedPoints'].view(bool),
        adjacent_cells=False,
    )


def imprint_to_volume(points):
    points = list(list(map(float, pt)) for pt in points)
    polygons_pts = pv.PolyData(points)
    mesh = polygons_pts.delaunay_2d()
    if len(mesh.points) != 0 and mesh.n_cells != 0:
        return mesh_to_volume(mesh)


def polygon_file_to_volume(polygon_file, output_dir):
    filename = f"{'.'.join(os.path.basename(polygon_file).split('.')[:-1])}.ply"
    output_path = os.path.join(output_dir, filename)
    if not os.path.exists(output_path):
        points = np.load(polygon_file).tolist()
        points = list(list(map(float, pt)) for pt in points)
        polygons_pts = pv.PolyData(points)
        mesh = polygons_pts.delaunay_2d()
        if len(mesh.points) != 0 and mesh.n_cells != 0 :
            vol = mesh_to_volume(mesh)
            if vol.n_open_edges == 0:  # if closed volume
                vol.save(output_path)
                return [polygon_file, output_path]
    else:
        return [polygon_file, output_path]


def list_imprint_to_list_volumes(track_list: list[str]):
    vol_list = []
    for track in track_list:
        points = np.load(track).tolist()
        vol = imprint_to_volume(points)
        vol_list.append(vol)
    return vol_list


def extract_all_points_in_volumes(point_cloud, v_list):
    result = None
    for vol in v_list:
        inside = extract_points_in_volume(point_cloud, vol)
        if inside.n_points != 0:
            closest_points = inside.extract_surface()
        else:
            closest_ids = point_cloud.find_closest_point(vol.center, 5)
            closest_points = point_cloud.extract_points(
                closest_ids,
                adjacent_cells=False,
            )
            closest_points = closest_points.extract_surface()

        if result is None:
            result = closest_points
        else:
            result += closest_points
            result = result.clean()
    return result


def point_cloud_stat_summary(point_cloud):
    names = point_cloud.array_names
    names = [x for x in names if not x.startswith("vtkOriginal")]
    metrics = []
    if point_cloud.number_of_points != 0:
        for name in names:  # For each metric
            point_cloud.set_active_scalars(name)
            sf = np.asarray(point_cloud[name])  # activate and retrieve the metric
            sf = sf[~np.isnan(sf)]  # Remove any nan values
            if len(sf) >= 2:
                # Summarise the metric
                m = mean(sf)
                sd = stdev(sf)
                med = median(sf)
                q1, q2, q3 = quantiles(sf)
            else:
                m = sd = med = q1 = q3 = np.nan
            metrics.append([name, m, sd, med, q1, q3])
    return metrics


def summarise_individual(point_cloud, individual_id: int, volume_file_list: list[str]):
    temp_file = f"temp_{individual_id}.txt"

    vol_list = [pv.read(file) for file in volume_file_list if file is not None]

    with open(temp_file, mode='w') as file:
        file.write(f"Volumes loaded: {len(vol_list)} vol to filter !")

    extracted_point_cloud = extract_all_points_in_volumes(point_cloud, vol_list)
    if extracted_point_cloud is None:
        os.remove(temp_file)
        return None

    with open(temp_file, mode='w') as file:
        file.write(f"Points extracted !")

    metrics = point_cloud_stat_summary(extracted_point_cloud)

    with open(temp_file, mode='w') as file:
        file.write(f"Summary done !")

    for m in metrics:
        m.append(individual_id)

    os.remove(temp_file)
    return metrics


def save_metrics(session, metrics):
    object_list = []
    for metric in metrics:
        if metric is not None:
            metric_full_name = metric[0]
            resolution = float(metric_full_name.split('_')[-2])
            metric_name = "_".join(metric_full_name.split('_')[:-2])
            object_list.append(rdb.geomorphometrics(
                metric=metric_name,
                resolution=resolution,
                mean=metric[1],
                sd=metric[2],
                median=metric[3],
                q1=metric[4],
                q3=metric[5],
                track_id=metric[6]
            ))
    session.bulk_save_objects(object_list)
    session.commit()
