from itertools import product
import numpy as np
import os


def search_contacts(vol_list_1, vol_list_2):
    volume_combination = list(product(range(len(vol_list_1)), range(len(vol_list_2))))
    for v_comb in volume_combination:
        v1 = vol_list_1[v_comb[0]]
        v2 = vol_list_2[v_comb[1]]

        p1 = np.array(v1.center)
        p2 = np.array(v2.center)
        squared_dist = np.sum((p1 - p2) ** 2, axis=0)
        dist = np.sqrt(squared_dist)

        if dist < 20:  # Only test if the dist_threshold between the two bounding boxes is less than 20 m
            k, intersection = v1.collision(v2, 1)
            if intersection:
                return 1
    return 0

def search_annotations_within(id, list_vols, non_tracked_individual_dict):
    temp_file = os.path.join(r"D:\tests\databases", f"temp_{id}.txt")
    res = []
    i = 0
    total = len(non_tracked_individual_dict)
    for nti_id, nti_polygons in non_tracked_individual_dict.items():
        with open(temp_file, mode='w') as file:
            file.write(f"Progress: {i}/{total}")
        i += 1
        b = 0
        for vol in list_vols:
            vol_center = np.array(vol.center)
            for poly in nti_polygons:
                poly_center = np.array(
                    [poly.bounds[1] - poly.bounds[0], poly.bounds[3] - poly.bounds[2], poly.bounds[5] - poly.bounds[4]])
                squared_dist = np.sum((vol_center - poly_center) ** 2, axis=0)
                dist = np.sqrt(squared_dist)
                if dist < 20:
                    try:
                        select = poly.select_enclosed_points(vol)
                        if np.sum(select['SelectedPoints']) != 0:
                            b = 1
                            break
                    except RuntimeError:
                        pass
            else:
                continue
            break
        if b==1:
            res.append(nti_id)

    os.remove(temp_file)
    return [id, res]
