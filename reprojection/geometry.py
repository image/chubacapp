import math
import numpy as np


def offset(c, d, bearing):
    x = c[0] + math.sin(bearing) * d
    y = c[1] + math.cos(bearing) * d
    return [x, y]


def circle_to_polygons(center, radius, edges=32):
    coordinates = []
    for i in range(edges):
        coordinates.extend(offset(center, radius, (2 * math.pi * i) / edges))
    coordinates.extend([coordinates[0], coordinates[1]])
    return coordinates


def rectangle_to_polygons(coords, edges=10):
    polygon = []
    for i, coord in enumerate(coords[:-1]):
        polygon.extend(coord)
        target = coords[i + 1]
        if coord[0] - target[0] != 0:
            dist = coord[0] - target[0]
            l = [[coord[0] + dist * (i / edges), coord[1]] for i in range(1, edges)]
        elif coord[1] - target[1] != 0:
            dist = coord[1] - target[1]
            l = [[coord[0], coord[1] + dist * (i / edges)] for i in range(1, edges)]
        else:
            l = []
        for j in l:
            polygon.extend(j)
    polygon.extend(coords[0])
    return polygon


def circle_3D_diameter(circle: list[list[float]]):
    dist = []
    for i, point in enumerate(circle):
        opposite_point = circle[(i + int(len(circle) / 2)) % len(circle)]
        dist.append(np.linalg.norm(np.array(point) - np.array(opposite_point)))
    return sum(dist) / len(dist)
