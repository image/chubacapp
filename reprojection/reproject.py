import camera
from scipy.spatial import distance
from openmvg_json_file_handler import OpenMVGJSONFileHandler
import pyembree
from reprojection.contour_finding import find_contour, get_contour_path, contour_finding_launcher
from reprojection_distance_filter import filter_hit_map
import itertools
import numpy as np
import trimesh
import os
import video_annotations
import ast
import logging
import geometry
import pathlib
from multiprocessing.pool import Pool
import pandas as pd
from tqdm import tqdm
from datetime import datetime
from PyQt5 import QtCore
import reprojection_database as rdb
import itertools
from pathlib import Path
import volume_filter as vf
import duplicates_buster


def batch_contour_finding(hit_map_dir):
    hit_map_list = [os.path.join(hit_map_dir, f) for f in pathlib.Path(hit_map_dir).iterdir() if f.is_file()]
    output_dir = os.path.join(hit_map_dir, 'contours')
    os.makedirs(output_dir, exist_ok=True)

    with Pool() as pool:
        pool.starmap(contour_finding_launcher, tqdm(zip(hit_map_list), total=len(hit_map_list)))


def save_3D_polygon(points_out, image_path, output_dir, track_id=None):
    image_name = '.'.join(os.path.basename(image_path).split('.')[:-1])
    if track_id is None:
        track_id = "contour"
    poly_filename = f"{image_name}_{str(track_id)}.npy"
    poly_path = os.path.join(output_dir, poly_filename)
    np.save(poly_path, points_out)
    return poly_path


def get_reproj_cameras(hit_maps_dir):
    hit_maps = os.listdir(hit_maps_dir)
    cam_list = {}
    for hm in hit_maps:
        if hm.endswith('.npy'):
            hm_path = os.path.join(hit_maps_dir, hm)
            img = hm.rsplit('.', maxsplit=1)[0]
            str_dt = img.rsplit('.', maxsplit=1)[0]
            date_object = datetime.strptime(str_dt, "%Y%m%dT%H%M%S.%fZ")
            cam_list[img] = {"datetime": date_object, "hm": hm_path}
    cam_df = pd.DataFrame.from_dict(cam_list, orient='index')
    cam_df['image_name'] = cam_df.index
    return cam_df


class AnnotationTo3d():
    def __init__(self, pc, hit_maps_dir, config):
        self.hit_maps_dir = hit_maps_dir
        self.pc = pc
        self.config = config
        self.config_name = self.config['name']

        self.reprojected_annotations_dir = self.config['output_path']

        self.reproj_cameras = get_reproj_cameras(self.hit_maps_dir)

        self.min_x = self.min_y = 0
        self.max_x = 0
        self.max_y = 0

        self.min_radius = 0.01

        self.session, self.db_path = rdb.open_reprojection_database_session(self.reprojected_annotations_dir,
                                                                            create=True)

    def point2hitpoint(self, ann_coords, hit_map):
        (x, y) = ann_coords
        if self.min_x <= x < self.max_x and self.min_y <= y < self.max_y:
            y = self.max_y - y  # Inverse Y axis
            coord = hit_map[int(x)][int(y)]
            if np.array_equal(coord, [0, 0, 0]):
                return None
            return coord
        return None

    def point_list2hit_point_list(self, vertexes: list[float], hit_map, decimation_factor=0):
        list_coord = list(zip(*[iter(vertexes)] * 2))
        hit_point_list = []
        misses_count = 0
        for i in list_coord[0::decimation_factor]:  # For all the points of the polygon
            # get the location of the intersection between ray and target
            coord = self.point2hitpoint((i[0], i[1]), hit_map)
            if coord is not None:
                hit_point_list.append([coord[0], coord[1], coord[2]])
            else:
                misses_count += 1
        return np.array(hit_point_list), misses_count

    def get_hit_map(self, hit_map_path):
        hit_map = np.load(hit_map_path)
        self.max_x, self.max_y = hit_map.shape[:2]
        contour_path = get_contour_path(hit_map_path)
        if os.path.isfile(contour_path):
            contour = np.load(contour_path)
        else:
            contour = find_contour(hit_map)
        contour = list(itertools.chain(*contour.tolist()))
        return hit_map, contour

    def reproject_annotations(self):
        if self.config_name == "annotation":
            if self.config["rep_type"] == "video":

                print("Retrieve video annotations tracks...")
                os.chdir(self.pc['project_directory'])
                logging.basicConfig(filename=self.pc['name'] + ".log", filemode="a")
                logging.info("Reprojection : Retrieve video annotations tracks...")

                annotations = video_annotations.get_annotations_tracks(self.config["rep_path"], self.reproj_cameras)
                annotations = annotations.rename(columns={"image_name": "filename"})
            else:
                annotations = pd.read_csv(self.config["rep_path"], sep=",")
                annotations['points'] = annotations['points'].apply(lambda x: ast.literal_eval(x))

        print("Starting reprojection...")
        os.chdir(self.pc['project_directory'])
        logging.basicConfig(filename=self.pc['name'] + ".log", filemode="a")
        logging.info("Reprojection : Retrieve video annotations tracks...")

        for image in tqdm(self.reproj_cameras['image_name']):
            if self.config_name == "annotation":
                ann_img = annotations.loc[annotations['filename'] == image]
                decimation_factor = 1
            elif self.config_name == "image_imprints":
                ann_img = pd.DataFrame(
                    columns=['filename', 'shape_name', 'points', 'label_name', 'label_hierarchy', 'annotation_id'])
                decimation_factor = 10
            else:
                return 0
            self.reproject(ann_img, image, False, decimation_factor)

        #  Computing volume with multiprocessing

        print("computing volumes...")
        list_coord_files = [r[0] for r in self.session.query(rdb.Annotations.coordinates_file).distinct()]
        volume_dir = os.path.join(self.reprojected_annotations_dir, "volumes")
        Path(volume_dir).mkdir(exist_ok=True)
        args = [[pol, volume_dir] for pol in list_coord_files]
        res = []
        with Pool() as pool:
            for result in pool.starmap(vf.polygon_file_to_volume, tqdm(args, total=len(args))):
                res.append(result)

        volume_dict = dict([i for i in res if i is not None])
        for key, value in volume_dict.items():
            (self.session.query(rdb.Annotations)
             .filter(rdb.Annotations.coordinates_file == key)
             .update({rdb.Annotations.volume_file: value}, synchronize_session=False))
            self.session.commit()

        # Remove polygons without volume
        (self.session.query(rdb.Annotations)
         .filter(rdb.Annotations.volume_file.is_(None))
         .delete(synchronize_session=False))
        self.session.commit()
        print("Volumes computed !")

        # Grouping as individuals
        print("Grouping as individuals")
        # Compute all contacts between annotations of same label
        duplicates_buster.get_contacts_per_labels(self.session)
        print("Contacts computed !")
        duplicates_buster.get_individuals(self.session)
        print("Individuals grouped !")
        self.session.commit()
        self.session.close()

        return self.reprojected_annotations_dir, self.db_path

    def reproject(self, annotations, image, label, decimation_factor):
        annotations_commits = []
        image_info = self.reproj_cameras[self.reproj_cameras['image_name'] == image].iloc[0]
        hit_map, contour = self.get_hit_map(image_info['hm'])
        if label:
            annotations['shape_name'] = 'WholeFrame'
            annotations['points'] = contour
            annotations['annotation_id'] = -999
        else:
            image_bound = pd.DataFrame({
                'filename': [image],
                'shape_name': ['WholeFrame'],
                'points': [contour],
                'label_name': ['bound'],
                'label_hierarchy': ['bound'],
                'annotation_id': [-999],
            })
            annotations = pd.concat([annotations, image_bound])

        image_db = rdb.Images(name=image, hit_map_path=image_info['hm'])
        self.session.merge(image_db)
        self.session.flush()

        points_out, misses_contour = self.point_list2hit_point_list(contour, hit_map, decimation_factor)
        misses_contour_percent = misses_contour / (len(contour) / 2)
        image_contour_path = save_3D_polygon(points_out, image, self.reprojected_annotations_dir)

        for index, ann in annotations.iterrows():  # for each annotation
            save = False
            shape_name = ann['shape_name']
            if ann['points'] is not None and shape_name != "WholeFrame":
                if ann['shape_name'] == 'Point':  # if the annotation is a point
                    vertexes = ann['points'][:2]
                elif shape_name == "Rectangle":
                    vertexes = list(zip(*[iter(ann['points'])] * 2))
                    vertexes = geometry.rectangle_to_polygons(vertexes)
                elif shape_name == "Circle":
                    x, y = ann['points'][:2]
                    r = ann['points'][2]
                    vertexes = geometry.circle_to_polygons((x, y), r)
                else:
                    vertexes = ann['points']
                points_out, misses = self.point_list2hit_point_list(vertexes, hit_map, decimation_factor)

                if points_out is not None:  # If we have a hit point
                    polygon_filename = save_3D_polygon(points_out, image, self.reprojected_annotations_dir,
                                                       ann['annotation_id'])
                    misses_percent = misses / (len(vertexes) / 2)
                    save = True

            elif shape_name == "WholeFrame":
                misses_percent = misses_contour_percent
                polygon_filename = image_contour_path
                save = True

            if save and misses_percent < 0.5:
                track = rdb.Tracks(id=ann['annotation_id'], label=ann['label_name'], type=shape_name,
                                   label_hierarchy=ann['label_hierarchy'])
                rdb.add_track(self.session, track)
                annotations_commits.append(
                    rdb.Annotations(
                        track_id=ann['annotation_id'],
                        type=ann['shape_name'],
                        coordinates_file=polygon_filename,
                        misses_percent=misses_percent,
                        image_name=image
                    )
                )

        for ann in annotations_commits:  # Saving change to annotation database
            self.session.merge(ann)
        self.session.commit()
        return 1


class reprojector(QtCore.QThread):
    prog_val = QtCore.pyqtSignal(int)
    finished = QtCore.pyqtSignal()

    def __init__(self, model, sfm, export):
        super(reprojector, self).__init__()
        self.running = True

        self.export = export
        mesh = trimesh.load(model)
        self.intersector = trimesh.ray.ray_pyembree.RayMeshIntersector(mesh)
        self.scene = trimesh.Scene([mesh])

        self.cams = OpenMVGJSONFileHandler.parse_openmvg_file(sfm, "NAME", True)

    def run(self):
        try:
            tot_len = len(self.cams)
            hit_map_correction_list = []
            for prog, cam in tqdm(enumerate(self.cams, start=1)):
                hit_map = self.convert_to_hit_map(cam)
                hm_path = self.save_hit_map(cam, hit_map)
                # hm_path = os.path.join(self.export, cam.get_relative_fp() + '.npy')
                hit_map_correction_list.append([hm_path, cam.get_camera_center()])
                self.prog_val.emit(round((prog / tot_len) * 100))
            print("Hitmaps generated")
            print("correcting hit maps for distance")
            with Pool() as pool:
                pool.starmap(filter_hit_map, tqdm(hit_map_correction_list, total=len(hit_map_correction_list)))
            print("Finding image contours")
            batch_contour_finding(self.export)
            print("Contours computed")

        except RuntimeError:
            print("An error occurred")
        self.prog_val.emit(0)
        self.finished.emit()
        self.running = False

    def save_hit_map(self, cam, hit_map):
        export_path = os.path.join(self.export, cam.get_relative_fp())
        np.save(export_path, hit_map)
        return export_path + '.npy'

    def convert_to_hit_map(self, cam):
        trsf_matrix, fov, shift, focal_length, res, dist = camera.get_cam_parameters(cam)

        self.scene.camera.resolution = res
        self.scene.camera.fov = np.rad2deg(fov)

        self.scene.camera_transform = trsf_matrix

        # convert the camera to rays with one ray per pixel
        origins, vectors, pixels = self.scene.camera_rays()
        points, index_ray, index_tri = self.intersector.intersects_location(
            origins, vectors, multiple_hits=False)
        # find pixel locations of actual hits
        pixel_ray = pixels[index_ray]
        # create a numpy array we can turn into an image
        # doing it with uint8 creates an `L` mode greyscale image
        a = np.zeros(np.append(res, 3), dtype=np.float16)
        a[pixel_ray[:, 0], pixel_ray[:, 1]] = points
        return a
