import numpy as np

def filter_hit_map(hm_path: str, center:np.ndarray, dist_threshold: float = 10):
    hit_map = np.load(hm_path)

    dist = np.linalg.norm(hit_map - center, axis=2)
    dist_mask = dist > dist_threshold

    hit_map[dist_mask] = np.zeros([3])
    np.save(hm_path, hit_map)
    return 1
