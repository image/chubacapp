import numpy as np
import pandas as pd
import sqlite3

MAX_IMAGE_ID = 2 ** 31 - 1


def get_score(M, keep):
    score = 0
    for i in range(len(keep)):
        if keep[i]:
            for j in range(len(keep)):
                if keep[j]:
                    score += M[i][j]
    return score - np.sum(keep)


def forward(M):
    keep = np.zeros(len(M))
    for i in range(len(M)):
        new_keep = np.copy(keep)
        new_keep[i] = 1
        score = get_score(M, new_keep)
        if score == 0:
            keep = np.copy(new_keep)
    return keep


def permutate(M):
    keep = np.ones(len(M))
    min_score = 99999
    while min_score > 0:
        min_i = 0
        min_score = 99999
        new_keep = np.copy(keep)
        for i in range(len(keep)):
            new_keep[i] = 0
            score = get_score(M, new_keep)
            if score < min_score:
                min_i = i
                min_score = score
            new_keep[i] = 1
        print("Score: {} . Ends at 0".format(min_score))
        keep[min_i] = 0
    return (keep)


def pair_id_to_image_ids(pair_id):
    image_id2 = pair_id % MAX_IMAGE_ID
    image_id1 = (pair_id - image_id2) / MAX_IMAGE_ID
    return int(image_id1), int(image_id2)


def get_contact_matrix(db_path, img_list):
    connection = sqlite3.connect(db_path)
    images_df = pd.read_sql_query("SELECT * FROM 'images'", connection)
    matches_df = pd.read_sql_query("SELECT * FROM 'matches'", connection)
    connection.close()

    matches_df['id1'], matches_df['id2'] = zip(*matches_df['pair_id'].map(pair_id_to_image_ids))
    matches_df = matches_df.drop(columns=['cols', 'data', 'pair_id'])

    images_df = images_df[["image_id", "name"]][images_df['name'].isin(img_list)]
    images_ids = images_df['image_id'].tolist()
    matches_df_filtered = matches_df[
        (matches_df['id1'].isin(images_ids))
        & (matches_df['id2'].isin(images_ids))
        & (matches_df['rows'] != 0)
        ]
    keep = np.zeros(len(images_ids)+1)
    for i, id in enumerate(images_ids):
        img_matches = matches_df_filtered[(matches_df_filtered['id1'] == id) | (matches_df_filtered['id2'] == id)]
        contacts = img_matches['id1'].tolist() + img_matches['id2'].tolist()
        contacts = [x for x in contacts if x != id]
        k = 1
        for contact in list(set(contacts)):
            if keep[contact]:
                k = 0
        if k:
            keep[id] = 1

    return keep
