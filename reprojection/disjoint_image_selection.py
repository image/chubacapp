from shutil import copy
import pathlib
import glob
import numpy as np
import os
import json
import imghdr
import pandas as pd
import utility
from scipy.spatial import distance_matrix
from PyQt5 import QtCore
import reprojection_database as rdb
from tqdm import tqdm

import volume_filter as vf
import disjoint_utils as du
from reconstruction.utils import extract_camera_points


class disjoint_launcher():
    def __init__(self, qt):
        self.qt = qt
        self.project_config = qt.project_config
        self.wholeframe_db_dir = self.project_config['outputs']['wholeframe_db_dir']
        self.models_list = self.project_config['outputs']['3D_models']
        model = self.models_list[0]
        sfm_path = model['sfm']
        input_file = open(sfm_path, "r")
        self.sfm = json.load(input_file)
        self.session = rdb.open_reprojection_database_session(self.wholeframe_db_dir, create=False)

    def launch_disjoint(self):
        self.reprojector = disjoint_image_selection(self.project_config, self.session, self.sfm)
        self.reprojector.finished.connect(self.end_disjoint)
        self.reprojector.start()

    def end_disjoint(self):
        print("finish disjoint")

class disjoint_image_selection(QtCore.QThread):
    prog_val = QtCore.pyqtSignal(int)
    finished = QtCore.pyqtSignal()

    def __init__(self, pc: dict, session, sfm: dict):
        super(disjoint_image_selection, self).__init__()
        self.running = True

        self.pc = pc
        self.session = session
        self.wholeframe_report = pd.read_sql(session.query(rdb.Annotations).statement, session.bind)

        self.reprojected_images_list = self.wholeframe_report["image_name"].to_list()

        self.disjoint_dir = pathlib.Path(self.pc['project_directory']) / "disjoint_image_selection"
        self.selected_image_dir = self.disjoint_dir / "selected_images"
        self.disjoint_dir.mkdir(parents=True, exist_ok=True)
        self.selected_image_dir.mkdir(parents=True, exist_ok=True)


        camera_points = extract_camera_points(sfm)
        camera_points = camera_points[camera_points['filename'].isin(self.reprojected_images_list)]
        self.cameras = pd.merge(self.wholeframe_report, camera_points, on='filename', how='left')
        self.dm = camera_points_distance_matrix(self.cameras)

    def run(self):
        try:
            self.selection("forward")
        except RuntimeError:
            print("An error occurred")
        self.prog_val.emit(0)
        self.finished.emit()
        self.running = False

    def selection(self, method: str):
        perimeters = [np.load(polygon).tolist() for polygon in self.wholeframe_report["coordinates_file"].to_list()]

        print("Getting contact matrix...")
        contact_matrix = self.get_contact_matrix(perimeters, self.dm)
        #contact_matrix = np.loadtxt(r"D:\test_chubacapp\disjoint_image_selection\contact_matrix.csv", delimiter=",")
        pd.DataFrame(contact_matrix).to_csv(self.disjoint_dir / "contact_matrix.csv", index=False, header=False)
        print("Done !")

        print("Image selection...")
        if method == "forward":
            keep = du.forward(contact_matrix)
        elif method == "permutation":
            keep = du.permutate(contact_matrix)
        else:
            print("Not a valid method, aborting...")
            return 0
        print("Done !")

        print(keep)
        print([i for (i, v) in zip(self.reprojected_images_list, keep) if v])

        filter_images(self.pc['inputs']['image_path'], self.selected_image_dir, keep, self.reprojected_images_list)
        print("Saved !")

        return 1

    def get_contact_matrix(self, perimeters, dm, dist_filter=5):
        vol_list = vf.list_imprint_to_list_volumes(perimeters)
        vol_len = len(vol_list)

        print("Starting contact analysis... {} images to analyse".format(vol_len))
        contact_matrix = np.zeros(shape=(vol_len, vol_len))
        for i in tqdm(range(vol_len)):
            for j in range(vol_len):
                if dm[i, j] < dist_filter and vol_list[i] is not None and vol_list[j] is not None:
                    k, intersection = vol_list[i].collision(vol_list[j], 1)
                    if intersection:
                        contact_matrix[i, j] = 1
        print("Done !")

        return contact_matrix


def camera_points_distance_matrix(camera_points):
    positions = []
    for index, row in camera_points.iterrows():
        positions.append([float(row['x']), float(row['y']), float(row['z'])])

    return distance_matrix(positions, positions)


def filter_images(input_dir, output_dir, keep, images_filenames):
    img_to_keep = [volume for i, volume in enumerate(images_filenames) if keep[i]]

    for file in os.listdir(input_dir):  # for each image in the directory
        filename = os.path.join(input_dir, file)
        if os.path.isfile(filename):  # Check if is a file
            if imghdr.what(filename) == "jpeg":
                if file in img_to_keep:
                    copy(filename, output_dir)


def list_image_in_model(dir, img_in_model):
    img_in_model_set = set(img_in_model)
    list_img = [file for file in os.scandir(dir) if
                file.is_file() and imghdr.what(file.path) == "jpeg" and file.name in img_in_model_set]
    return list_img
