import reprojection_database as rdb
from itertools import chain
import pyvista as pv
from contacts_processes import search_annotations_within
from tqdm import tqdm
from multiprocessing.pool import Pool
import pickle
from collections import Counter
import numpy as np
import pandas as pd
import os


def get_tracks_non_tracks(individuals):
    tracked_individual_dict = {}
    non_tracked_individual_dict = {}
    for ind in tqdm(individuals):  # Pour chaque individus
        tracks = ind.tracks
        if len(tracks) != 0:
            # Not possible for bounds and for points or polylines
            if tracks[0].label != "bound" and tracks[0].type not in ["Point, PolyLine"]:

                list_annotations = list(chain.from_iterable([t.annotations for t in tracks]))
                if len(list_annotations) > len(tracks):  # Tracked
                    tracked_individual_dict[ind.id] = [pv.read(i.volume_file) for i in list_annotations if
                                                       i.volume_file is not None]
                else:  # Not tracked
                    non_tracked_individual_dict[ind.id] = [pv.PolyData(np.load(i.coordinates_file).astype('float')) for
                                                           i in list_annotations if i.volume_file is not None]

    return tracked_individual_dict, non_tracked_individual_dict


def individuals_in_tracked_annotations(session, output_dir: str):
    individuals = session.query(rdb.individual).all()

    tracked_individual_dict, non_tracked_individual_dict = get_tracks_non_tracks(individuals)

    args = [[tracked_ind_id, tracked_ind_vols, non_tracked_individual_dict] for tracked_ind_id, tracked_ind_vols in
            tracked_individual_dict.items()]
    res = []
    with Pool() as pool:
        for r in pool.starmap(search_annotations_within, tqdm(args, total=len(args))):
            res.append(r)

    count_list = []
    for ind in res:
        contacts_labels = [session.get(rdb.individual, i).tracks[0].label for i in ind[1]]

        count = {"id": ind[0], "label": session.get(rdb.individual, ind[0]).tracks[0].label}

        count.update(dict(Counter(contacts_labels)))
        count_list.append(count)

    count_pd = pd.DataFrame(count_list)

    with open(os.path.join(output_dir, "output_count.pkl"), 'wb') as fp:
        pickle.dump(count_pd, fp)

    return count_pd





if __name__ == '__main__':
    print("computing volumes...")
    db_dir = r"D:\tests\databases"
    output_dir = r"D:\tests\databases"

    session, db_path = rdb.open_reprojection_database_session(db_dir, create=False)
    result = individuals_in_tracked_annotations(session, output_dir)

    print("ok")