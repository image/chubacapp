import os
import volume_filter as vf
from UI.summary_statistics_ui import Ui_Dialog
from PyQt5.QtWidgets import (QDialog)
import pandas as pd
import utility
from pathlib import Path
from pv_utils import parse_point_clouds
from multiprocessing import Pool
import reprojection_database as rdb
from itertools import chain
from tqdm import tqdm


class VolStatDialog(QDialog, Ui_Dialog):
    def __init__(self, qt):
        super().__init__()
        self.setupUi(self)
        self.qt = qt
        self.project_config = qt.project_config

        self.annotations_list = self.project_config['outputs']['reprojected_annotations']
        annotations_name_list = [ann["report_name"] for ann in self.annotations_list]
        self.reproj_annot.addItems(annotations_name_list)
        self.buttonBox.accepted.connect(self.launch_summary_stat)

        self.models_list = self.project_config['outputs']['3D_models']
        if len(self.models_list) != 0:
            item_list = [x['name'] for x in self.models_list]
            self.model_3D.addItems(item_list)

    def launch_summary_stat(self):
        video_tracks_only = self.video_tracks_only.isChecked()
        reproj_annotations_name = self.reproj_annot.currentText()
        reproj_annotations = next(
            (item for item in self.annotations_list if item["report_name"] == reproj_annotations_name), None)
        model_name = self.model_3D.currentText()
        model = next((item for item in self.models_list if item["name"] == model_name), None)
        geomorphometrics = self.project_config["outputs"]["geomorphometrics"]
        output_dir = utility.create_dir(os.path.join(self.project_config['project_directory'], "volume_stat"))

        session, db_path = rdb.open_reprojection_database_session(
            reproj_annotations["reprojected_annotation_dir"],
            create=False
        )

        stat_summary(geomorphometrics, session, output_dir, video_tracks_only)
        session.close()


def stat_summary(geomorphometrics, session, output_dir, vid_tracks_only):
    list_point_cloud = parse_point_clouds(geomorphometrics)

    # Delete previous geomorphometrics
    session.query(rdb.geomorphometrics).delete()
    session.commit()

    individuals = session.query(rdb.individual).all()

    individual_dict = {}
    for ind in tqdm(individuals):  # Pour chaque individus
        tracks = ind.tracks
        if len(tracks) != 0:
            if tracks[0].label != "bound" and tracks[0].type not in ["Point, PolyLine"]:
                # Not possible for bounds and for points or polylines
                list_annotations = list(chain.from_iterable([t.annotations for t in tracks]))
                if len(list_annotations) == len(tracks):
                    individual_dict[ind.id] = [i.volume_file for i in list_annotations if i.volume_file is not None]


    for point_cloud in list_point_cloud:
        individual_summary = []
        args = [[point_cloud, i, volume_list] for i, volume_list in individual_dict.items()]
        # for x, y, z in args:
        #     vf.summarise_individual(x, y, z)
        with Pool() as pool:
            for result in pool.starmap(vf.summarise_individual, tqdm(args, total=len(args))):
                if result is not None:
                    individual_summary.extend(result)
        print("Saving summary")
        vf.save_metrics(session, individual_summary)

    return 1



        # if not vid_tracks_only and len(non_track_ids) != 0:
        #     print("Polygon summary...")
        #     ann = [[point_cloud, annotations.loc[annotations['track_id'] == i].squeeze().to_dict()] for i in
        #            non_track_ids]
        #     with Pool() as pool:
        #         for result in pool.starmap(vf.summarise_polygon, tqdm(ann, total=len(ann))):
        #             if result is not None:
        #                 polygon_summary.extend(result)
        #     print("Saving polygon summary")
        #     vf.save_metrics(session, polygon_summary)
        # if len(track_ids) != 0:
        #     print("track summary")
        #     tracks = [[point_cloud, annotations[annotations['track_id'] == i]] for i in track_ids]
        #     with Pool() as pool:
        #         for result in pool.starmap(vf.summarise_track, tqdm(tracks, total=len(tracks))):
        #             tracks_summary.extend(result)
        #     print("Saving track summary")
        #     vf.save_metrics(session, tracks_summary)

