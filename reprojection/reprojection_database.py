from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, DateTime, ForeignKey, Float
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref
import os

Base = declarative_base()


class Images(Base):
    __tablename__ = 'Images'

    name = Column(String(50), primary_key=True)
    hit_map_path = Column(String(100))
    annotations = relationship("Annotations", backref="image")


class Contacts(Base):
    __tablename__ = 'Contacts'
    track_1_id = Column(Integer, ForeignKey('Tracks.id'), primary_key=True)
    track_2_id = Column(Integer, ForeignKey('Tracks.id'), primary_key=True)
    track_1 = relationship("Tracks", backref="contact_1",  foreign_keys=[track_1_id])
    track_2 = relationship("Tracks", backref="contact_2",  foreign_keys=[track_2_id])


class Annotations(Base):
    __tablename__ = 'Annotations'

    id = Column(Integer, primary_key=True)
    type = Column(String(20))

    coordinates_file = Column(String(60))
    misses_percent = Column(Float)
    volume_file = Column(String(60), nullable=True)

    track_id = Column(Integer, ForeignKey('Tracks.id'))
    image_name = Column(String(50), ForeignKey('Images.name'))


class Tracks(Base):
    __tablename__ = 'Tracks'
    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    label = Column(String(60))
    label_hierarchy = Column(String(100))
    geomorphometrics = relationship("geomorphometrics", backref="track")
    annotations = relationship("Annotations", backref="track")
    individual_id = Column(Integer, ForeignKey("individual.id"), nullable=True)


class individual(Base):
    __tablename__ = 'individual'
    id = Column(Integer, primary_key=True)
    tracks = relationship("Tracks", backref="individual")


class geomorphometrics(Base):
    __tablename__ = 'geomorphometrics'
    id = Column(Integer, primary_key=True)
    metric = Column(String(60))
    resolution = Column(Float)
    mean = Column(Float)
    sd = Column(Float)
    median = Column(Float)
    q1 = Column(Float)
    q3 = Column(Float)
    track_id = Column(Integer, ForeignKey('Tracks.id'))


def open_reprojection_database_session(db_dir, create: bool):
    db_path = os.path.join(db_dir, 'reprojection.db')
    if create and os.path.exists(db_path):
        os.remove(db_path)
    engine = create_engine(f'sqlite:///{db_path}', echo=False)
    if create:
        Base.metadata.create_all(engine)
    return sessionmaker(bind=engine, autoflush=True)(), db_path


def add_track(session, track):
    exists = session.query(Tracks).filter_by(id=track.id).first()
    if not exists:
        session.add(track)
        session.commit()


if __name__ == '__main__':
    db_dir = r"D:\tests\databases"
    session, path = open_reprojection_database_session(db_dir, False)

    print("session")
